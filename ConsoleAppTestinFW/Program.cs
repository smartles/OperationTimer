﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleAppTestinFW
{
    class Program
    {
        static void Main(string[] args)
        {
            Int32 count = 1000000;

            using (new OperationTimer.OperationTimer("List<Int32>"))
            {
                List<Int32> list = new List<Int32>();
                for(Int32 i = 0; i < count; i++ )
                {
                    list.Add(count);
                    Int32 x = list[i];
                }
                list = null;
            }

            using (new OperationTimer.OperationTimer("ArrayList"))
            {
                ArrayList list = new ArrayList();
                for (Int32 i = 0; i < count; i++)
                {
                    list.Add(count);
                    Int32 x = (Int32)list[i];
                }
                list = null;
            }

            Console.ReadLine();
        }
    }
}
