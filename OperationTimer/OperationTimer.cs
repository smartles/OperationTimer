﻿using System;
using System.Diagnostics;

namespace OperationTimer
{
    public sealed class OperationTimer : IDisposable
    {
        private Stopwatch m_startTime;
        private String m_text;
        private Int32 m_collectionCount;
        public OperationTimer(String text)
        {
            PrepareForOperation();
            m_text = text;
            m_collectionCount = GC.CollectionCount(0);
            m_startTime = Stopwatch.StartNew();
        }
        public void Dispose()
        {
            Console.WriteLine($"{m_startTime.Elapsed} (GCs={GC.CollectionCount(0)},{m_text}) {m_collectionCount}");
        }
        private static void PrepareForOperation()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }

}
